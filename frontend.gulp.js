'use strict';
const gulp = require('gulp');
var browserSync = require('browser-sync').create();
var path = require('path');
var gutil = require('gulp-util');
const replace = require('gulp-replace');
const concat = require('gulp-concat');
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var nestedcss = require('postcss-nested');
var postCssImport = require('postcss-import');
var cssNano = require('cssnano');
var rename = require('gulp-rename');
var webpackStream = require('webpack-stream');
const webpack = require('webpack');
const webp = require('gulp-webp');
const plumber = require('gulp-plumber');
const process = require('process');
const paths = require('./paths');

gulp.task('webserver', ['js', 'post-css'], () => {
    browserSync.init({
        proxy: "http://localhost:6002",
        port: 7071,
        files: ['./public/**/*'],
        https: false,
        online: true,
        open: false,
        reloadDebounce: 1000,
        reloadDelay: 150,
    });
    gulp.watch('frontend/css/*.css', ['frontend-css']);
});

let buildCss = (sourceDir, targetDir, stylesheetName) => {
    return function () {
        var processors = [
            postCssImport,
            nestedcss,
            autoprefixer({browsers: ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4']}),
            cssNano,
        ];
        return gulp.src(sourceDir + '/index.css')
        .pipe(sourcemaps.init())
        .pipe(postcss(processors))
        .pipe(rename(stylesheetName))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(targetDir));
    }
}
gulp.task('frontend-css', buildCss(paths.styles.sourceDir, paths.buildDir, 'frontend.css'));

gulp.task('post-css', ['frontend-css'], () => {
    return gulp.src(paths.buildDir + '/*.{css,css.map}')
    .pipe(gulp.dest(paths.styles.targetDir));
});


gulp.task('js', ['frontend-js']);

gulp.task('frontend-js', () => { //watch
    let webpackConfig = require('./frontend.webpack2.config.js');
    webpackConfig.watch = process.env.NODE_ENV !== 'production';
    gulp.src(paths.frontend.sourceDir + 'entryPoint.jsx')
    .pipe(plumber())
    .pipe(webpackStream(webpackConfig, webpack))
    .pipe(rename('frontend.min.js'))
    .pipe(gulp.dest(paths.frontend.targetDir));
});

gulp.task('default', ['webserver']);
gulp.task('build', ['js', 'frontend-css']);