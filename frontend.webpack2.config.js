'use strict';
const webpack = require('webpack');
const path = require('path');
const process = require('process');

const paths = require('./paths');

const APP_DIR = path.resolve(__dirname, paths.frontend.sourceDir);
let config = {
    entry: {
        app: ['core-js/fn/promise', 'core-js/fn/object/keys', APP_DIR + '/entryPoint.jsx']
    },
    output: {
        filename: 'frontend.js',
        path: path.join(__dirname, paths.frontend.targetDir),
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    cacheDirectory: true,
                    presets: ["react", "es2015", "stage-0"],
                },
            },
        ],
    },

    plugins: [],
};
module.exports = config;
if (process.env.NODE_ENV === 'production') {
    module.exports.devtool = '#source-map'
    // http://vue-loader.vuejs.org/en/workflow/production.html
    module.exports.plugins = (module.exports.plugins || []).concat([
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: false,
            compress: {
                warnings: false
            }
        }),

        new webpack.LoaderOptionsPlugin({
            minimize: true
        })
    ]);
}