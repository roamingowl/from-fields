'use strict'
import React from 'react';
import PropTypes from 'prop-types';
import EventBus from 'wolfy87-eventemitter';

import Field from './../../../src/field';

export default class FormField extends React.Component {
    constructor(props) {
        super(props);
        this.name = props.name;
        this.field = new Field(props.name, this.props.validators, {eventEmitter: new EventBus()}); //todo use singleton for all fields!
        console.log('we have props', this.props)
        if (this.props.parent) {
            this.props.parent.addChild(this.field);
        }
        this.state = {
            parent: this.props.parent,
            value: this.props.value,
            valid: undefined,
            invalidMessage: this.props.invalidMessage,
            validating: false,
            changed: false,
            touched: false
        }
        console.log('state', this.state);
    }

    componentWillReceiveProps(newProps) {
        if (this.value !== newProps.value) {
            this.setState({value: newProps.value});
        }
    }

    componentDidMount() {
        if (this.props.onChange) {
            this.field.addListener(this.field.CHANGE_EVENT_NAME, this.props.onChange);
            if (this.props.parent) {
                console.log('add listenener ', this.props.parentOnChange);
                //this.field.addListener(this.field.CHANGE_EVENT_NAME, this.props.parentOnChange)
                this.field.addListener(this.field.AFTER_VALIDATE_EVENT_NAME, this.props.parentOnValidationEnd);
            }
        }
        this.field.addListener(this.field.BEFORE_VALIDATE_EVENT_NAME, this.beforeValidate);
        this.field.addListener(this.field.AFTER_VALIDATE_EVENT_NAME, this.afterValidate);
        //this.props.form.add(this.props.name, Field)
    }

    beforeValidate = () => {
        this.setState({validating: true})
    }

    afterValidate = () => {
        this.setState({validating: false})
    }

    componentWillUnmount() {
        this.field.destruct() //TODO: rename destruct to unregisterAllListeners
    }

    onChange = (evt) => {
        let value;
        if (evt.target && evt.target.value !== undefined) {
            value = evt.target.value;
        } else {
            value = evt;
        }
        if (this.field.value === value) {
            return;
        }
        this.field.setValue(value)
        .then(() => {
            this.setState({value: value});
            return this.field.validate();
        })
        .then((valid) => {
            this.setValidationResult(valid, true);
        })
    }

    setValidationResult = (valid, changed) => {
        console.log('set val result', valid, changed);
        let newState = {valid: valid, changed: changed};
        if (!valid) {
            newState.invalidMessage = this.state.invalidMessage;
        }
        this.setState(newState);
    }

    onBlur = () => {
        this.setState({touched: true}, () => {
            if (this.state.value !== undefined) {
                this.field.validate()
                .then((valid) => {
                    this.setValidationResult(valid, this.state.changed);
                })
            }
        })
    }

    render() {
        if (!this.props.children) {
            return null;
        }
        return React.cloneElement(this.props.children, {
            onChange: this.onChange,
            onBlur: this.onBlur,
            changed: this.state.changed,
            touched: this.state.touched,
            valid: this.state.valid,
            value: this.state.value,
            validating: this.state.validating,
            invalidMessage: this.state.invalidMessage
        });
    }
}

Field.propTypes = {
    name: PropTypes.string.isRequired,
    children: PropTypes.element.isRequired
};