'use strict'
import React from 'react';
import JsField from './../../../src/field';
import Field from './Field';
import EventBus from 'wolfy87-eventemitter';

// class FormController {
//     constructor() {
//         this.fields = {}
//     }
//
//     remove(name) {
//         if (this.fields[name]) {
//             console.log('filed ' + name + ' deleted!')
//             delete this.fields[name];
//         }
//     }
//
//     add(name, field) {
//         if (!(field instanceof Field)) {
//             console.warn('Attempting to add non-field "' + name + '" to form!', field);
//             return;
//         }
//         this.fields[name] = field;
//     }
// }

export default class Group extends React.Component {
    constructor(props) {
        super(props);
        console.log('Form ' + this.props.name + ' has ' + this.props.children.length + ' children')
        this.groupField = new JsField(this.props.name);
        this.state = {
            valid: undefined
        }
    }

    setValidationResult = (valid) => {
        console.log('set val result to group', valid);
        let newState = {valid: valid};
        this.setState(newState);
    }

    componentDidMount() {
        setTimeout(() => {
            this.groupField.validate();
        },3000);
    }

    onChange = (childName, value) => {
        // console.log('children change', childName, value);
        // let promise =  this.groupField.isValid()
        // if (promise.promise) {
        //     promise = promise.promise;
        // }
        // console.log('ch promsie', promise);
        // promise.then((valid) => {
        //     console.log('we have group result', valid);
        //     this.setValidationResult(valid);
        // })
        // .catch((err) => {
        //     if (err.isCanceled) {
        //         return;
        //     }
        // })
        // return promise;
    }

    onChildValidationEnd = (valid) => {
        console.log('child validation result', valid);
        this.setValidationResult(valid);
    }

    render() {
        return (<div>
            {this.getChildern(this.props.children)}
        </div>)
    }

    getChildern(children) {
        return React.Children.map(children, (child) => {
            let extraProps = {
                parent: this.groupField,
                parentOnChange: this.onChange,
                parentOnValidationEnd: this.onChildValidationEnd
            }
            if (!React.isValidElement(child)) {
                return child;
            }

            if (child.type === Field) {
                return React.cloneElement(child, Object.assign({}, child.props, extraProps));
            }
            if (child.props.children) {
                console.log('we have children');
                return React.cloneElement(child, {
                    children: this.getChildern(child.props.children)
                });
            }
            console.log('CH', Object.assign({}, child.props, extraProps), child);
            return React.cloneElement(child, Object.assign({}, child.props, extraProps));
        });
    }
}