'use strict';

import React from 'react';

export default class Field extends React.Component {
    constructor(props) {
        super(props)
    }

    render () {
        let WrappedComponent = this.props.children;
        let props = this.props;
        return React.cloneElement(this.props.children, props);
    }
}