'use strict'
import React from 'react';

import FormField from './components/form/Field'
import Group from "./components/form/Group";


class SubComponent extends React.Component {
    render() {
        console.log('sc!', this.props.form);
        return (<div>
            <div>b</div>
            <Field form={this.props.form} name="field3-sub">
                <input />
            </Field>
            <div>e</div>
        </div>)
    }
}

class FirstName extends React.Component {
    render() {
        return (
            <div>
                First name:<br/>
                <input onBlur={this.props.onBlur} onChange={this.props.onChange} value={this.props.value}/>
                <br/>
                {this.props.validating ? <span>...</span>
                    :
                    (this.props.touched || this.props.changed) &&
                    !this.props.valid ? <span>!!!{this.props.invalidMessage}</span> : <span>ok</span>
                }
            </div>
        );
    }
}

export default class MainPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            firstName: 'zzz'
        }
    }

    showField = (evt, val) => {
        this.setState({show: evt.target.checked})
    }

    onFirstNameChange = (val) => {
        this.setState({firstName: val});
    }

    onFieldChange = (value) => {
        this.setState({firstName: value});
    }

    render() {
        return (
            <div>
                <Group name="g1">
                    Form example:
                    <div>
                        <FormField invalidMessage="firstname is not valid!" onChange={this.onFieldChange}
                                   name="firstName"
                                   value={this.state.firstName} validators={[new class {
                            validate(value) {
                                return new Promise((resolve, reject) => {
                                    setTimeout(() => {
                                        resolve(value === 'abc')
                                    }, 1000)
                                })
                            }
                        }]}>
                            <FirstName />{/*onChange, valid, invalidMessage*/}
                        </FormField>
                    </div>
                </Group>
            </div>
        )
    }
}