'use strict';
module.exports = {
    frontend: {
        sourceDir: 'frontend',
        targetDir: 'public/js'
    },
    buildDir: './build/',
    styles: {
        targetDir: './public/css',
        sourceDir: './frontend/css',
    }
}
