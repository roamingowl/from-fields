'use strict';

require('babel-core/register')({
    presets: ['react', 'es2015']
});

const express = require('express');
const hbs = require('hbs');
const path = require('path');
const hbsutils = require('hbs-utils')(hbs);
const reactServer = require('react-dom/server');
const react = require('react');

let app = express();
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, path.sep + 'templates'));
hbsutils.registerPartials(path.join(__dirname, path.sep + 'templates' + path.sep + 'partials'));

let file = require('./frontend/field.jsx');
let pageHeader = require('./frontend/components/pageHeader.jsx').default;

app.get('/', function (req, res) {
    //let cmp1 = react.createFactory(headerComponent.default);
    //res.render('index', {headerComponent: reactServer.renderToString(react.createFactory(file.default)({children: cmp1, label: 'aaaa', secret: 'sec'}))});
    res.render('index', {pageHeader: reactServer.renderToString(react.createFactory(pageHeader)({/*???props*/}))});
});

app.use('/js', express.static('public/js'
));

app.listen(6002, (err) => {
    console.log('Listening on 6001');
})