'use strict';
const debug = require('debug')('field');

const makeCancelable = (promise) => {
    let hasCanceled_ = false;

    const wrappedPromise = new Promise((resolve, reject) => {
        promise.then((val) =>
            hasCanceled_ ? reject({isCanceled: true}) : resolve(val)
        );
        promise.catch((error) =>
            hasCanceled_ ? reject({isCanceled: true}) : reject(error)
        );
    });

    return {
        promise: wrappedPromise,
        cancel() {
            hasCanceled_ = true;
        },
    };
};

module.exports = class Field {
    constructor(name, validators, options) {
        this.name = name;
        this.children = {};
        this.validators = [];
        if (validators && validators.length > 0) {
            for (let i = 0; i < validators.length; i++) {
                if (validators[i] && (typeof validators[i] === 'function' || (validators[i].validate && typeof validators[i].validate === 'function'))) {
                    this.validators.push(validators[i]);
                } else {
                    debug('Validator does not have a compatible interface');
                }
            }
        }
        if (options && options.eventEmitter) {
            this.$eventEmitter = options.eventEmitter;
        }
        this.CHANGE_EVENT_NAME = 'change';
        this.BEFORE_VALIDATE_EVENT_NAME = 'before-validate';
        this.AFTER_VALIDATE_EVENT_NAME = 'after-validate';
        if (options && options.prefixEventNames === true) {
            this.CHANGE_EVENT_NAME = 'field-' + this.name + '-' + this.CHANGE_EVENT_NAME;
            this.BEFORE_VALIDATE_EVENT_NAME = 'field-' + this.name + '-' + this.BEFORE_VALIDATE_EVENT_NAME;
            this.AFTER_VALIDATE_EVENT_NAME = 'field-' + this.name + '-' + this.AFTER_VALIDATE_EVENT_NAME;
        }
        this.value;
        this.valid;
        this.validating = false;
    }

    addChild(field) {
        if (!(field instanceof Field)) {
            console.warn('Child is not instance of Field')
            return;
        }
        this.children[field.name] = field;
    }

    destruct() {
        if (!this.$eventEmitter || !this.$eventEmitter.removeAllListeners) {
            return;
        }
        this.$eventEmitter.removeAllListeners(this.CHANGE_EVENT_NAME);
        this.$eventEmitter.removeAllListeners(this.BEFORE_VALIDATE_EVENT_NAME);
        this.$eventEmitter.removeAllListeners(this.AFTER_VALIDATE_EVENT_NAME);
    }

    validate(callback) {
        this.validating = true;
        console.log('we have evnt emiter', this.name, this.$eventEmitter, this.BEFORE_VALIDATE_EVENT_NAME, this.value);
        this.$eventEmitter && this.$eventEmitter.emit && this.$eventEmitter.emit(this.BEFORE_VALIDATE_EVENT_NAME, this.value);
        let validationResult;
        if (this.validationPromise) {
            console.log('got promise already!')
            validationResult = this.validationPromise;
        } else {
            validationResult = this.isValid();
        }

        if (validationResult.then && typeof validationResult.then === 'function') {
            return validationResult.then((valid) => {
                this.valid = valid;
                this.$eventEmitter && this.$eventEmitter.emit && this.$eventEmitter.emit(this.AFTER_VALIDATE_EVENT_NAME, valid);
                this.validating = false;
                debug('We have callback ', callback);
                if (callback && typeof callback === 'function') {
                    callback(valid)
                }
                delete this.validationPromise
                return valid;
            })
            .catch((err) => {
                if (err.isCanceled === true) {
                    delete this.validationPromise
                    return;
                }
            })
        }
        this.valid = validationResult;
        this.$eventEmitter && this.$eventEmitter.emit && this.$eventEmitter.emit(this.AFTER_VALIDATE_EVENT_NAME, validationResult);
        this.validating = false;
        if (callback && typeof callback === 'function') {
            callback(this.valid)
        }
        delete this.validationPromise;
        return this.valid;
    }

    isValid() {
        let fieldValue = this.value;
        console.trace('start validation for ', this.name, this.value);
        let results = [];
        let usePromise = false;
        if (this.children && Object.keys(this.children).length > 0) {
            for (let childName of Object.keys(this.children)) {
                let validationResult
                if (this.children[childName].validationPromise) {
                    validationResult = this.children[childName].validationPromise;
                } else {
                    validationResult = this.children[childName].validate();
                }
                results.push(validationResult);
                if (validationResult.then && typeof validationResult.then === 'function') {
                    usePromise = true;
                }
            }
        } else {
            for (let i = 0; i < this.validators.length; i++) {
                let validationResult;
                if (typeof this.validators[i] === 'function') {
                    validationResult = this.validators[i](fieldValue);
                } else {
                    validationResult = this.validators[i].validate(fieldValue);
                }
                results.push(validationResult);
                if (validationResult.then && typeof validationResult.then === 'function') {
                    usePromise = true;
                }
            }
        }

        if (usePromise) {
            debug('One of validators returned a promise', results);
            if (this.validationPromise) {
                debug('some validation in progress!');
                this.validationPromise.cancel();
            }
            this.validationPromise = makeCancelable(Promise.all(results)
                .then((results) => {
                    debug('got results', results, 'for value ', fieldValue);
                    return results.reduce((prev, cur) => {
                        return prev && cur
                    });
                })
            );
            return this.validationPromise.promise
        }
        debug('All results are simple (' + this.name + ')', results);
        return results.reduce((prev, cur) => {
            return prev && cur
        });
    }

    //TODO: needed??
    // static create(name) {
    //     return new Field(name);
    // }
    //
    // static createEmittable(name, emitter) {
    //     return new Field(name, {eventEmitter: emitter});
    // }

    once() { //alias
        this.addOnceListener(...arguments)
    }

    addOnceListener() {
        if (!this.$eventEmitter || !this.$eventEmitter.addOnceListener) {
            return;
        }
        this.$eventEmitter.addOnceListener(...arguments)
    }

    on() { //alias
        this.addListener(...arguments)
    }

    addListener() {
        if (!this.$eventEmitter || !this.$eventEmitter.addListener) {
            return;
        }
        this.$eventEmitter.addListener(...arguments)
    }

    setValue(value, callback) {
        this.value = value;
        return new Promise((resolve) => {
            this.$eventEmitter && this.$eventEmitter.emit && this.$eventEmitter.emit(this.CHANGE_EVENT_NAME, value)
            if (callback) {
                callback(value)
                return resolve(value);
            }
            return resolve(value);
        })
    }
}