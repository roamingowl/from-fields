'use strict';
const assert = require('chai').assert;
const Field = require('./../../src/field');
const _ = require('lodash');

describe('async/mixed validators', function () {
    it('should set validation result to field (mixed validators) - invalid value', function (done) {
        let validators = [
            new class isStringValidator {
                validate(value) {
                    return Promise.resolve(_.isString(value));
                }
            },
            function (value) {
                return value !== 'badString';
            }
        ];
        let field = new Field('testField', validators);
        field.setValue(666)
        assert.equal(field.value, 666);
        let promise = field.validate();
        assert.isTrue(typeof promise.then === 'function');
        promise.then((valid) => {
            assert.isFalse(valid);
            assert.isFalse(field.valid);
            done()
        })
    })
    it('should return validation result in a promise (mixed validators, random validation time)', function (done) {
        let validators = [
            new class specificValidator {
                validate(value) {
                    return new Promise((resolve) => {
                            setTimeout(() => {
                                resolve(value === 1);
                            }, value * 100)
                        }
                    );
                }
            },
        ];
        let filed = new Field('testField', validators);
        let promises = [];
        filed.setValue(4);
        promises.push(filed.validate());
        filed.setValue(3);
        promises.push(filed.validate());
        filed.setValue(1);
        promises.push(filed.validate());
        Promise.all(promises).then(() => {
            assert.equal(filed.valid, true);
            done()
        })
        .catch((err) => {
            done(err);
        })
    });
    it('should return validation result in a promise (mixed validators) - hierarchy of fields', function (done) {
        let subValidators1 = [
            new class isStringValidator {
                validate(value) {
                    return Promise.resolve(_.isString(value));
                }
            },
        ];
        let subValidators3 = [
            new class isFiveValidator {
                validate(value) {
                    return Promise.resolve(value === 5);
                }
            },
        ];
        let groupFiled = new Field('testField');
        let subField1 = new Field('subField1', subValidators1);
        let subField2 = new Field('subField2');
        let subSubField1 =new Field('subsSubField1', subValidators3)
        groupFiled.addChild(subField1);
        groupFiled.addChild(subField2);
        subField2.addChild(subSubField1);
        subField1.setValue('stringVal')
        subSubField1.setValue(5);

        groupFiled.isValid()
        .then((valid) => {
            assert.equal(valid, true);
            done()
        })
        .catch((err) => {
            done(err)
        })
    })
    it('should return validation result in a promise (mixed validators) - invalid value', function (done) {
        let validators = [
            new class isStringValidator {
                validate(value) {
                    return Promise.resolve(_.isString(value));
                }
            },
            function (value) {
                return value !== 'badString';
            }
        ];
        let field = new Field('testField', validators);
        field.setValue(666)
        assert.equal(field.value, 666);
        let promise = field.isValid();
        assert.isTrue(typeof promise.then === 'function');
        promise.then((valid) => {
            assert.isFalse(valid);
            assert.isUndefined(field.valid);
            done()
        })
    });
    it('should return validation result in a promise - invalid value', function (done) {
        let validators = [
            new class isStringValidator {
                validate(value) {
                    return Promise.resolve(_.isString(value));
                }
            },
            function (value) {
                return Promise.resolve(value !== 'badString');
            }
        ];
        let field = new Field('testField', validators);
        field.setValue(666)
        assert.equal(field.value, 666);
        let promise = field.isValid();
        assert.isTrue(typeof promise.then === 'function');
        promise.then((valid) => {
            assert.isFalse(valid);
            assert.isUndefined(field.valid);
            done()
        })
    });
    it('should return validation result in a promise - valid value', function (done) {
        let validators = [
            new class isStringValidator {
                validate(value) {
                    return Promise.resolve(_.isString(value));
                }
            },
            function (value) {
                return Promise.resolve(value !== 'badString');
            }
        ];
        let field = new Field('testField', validators);
        field.setValue('goodString')
        assert.equal(field.value, 'goodString');
        let promise = field.isValid();
        assert.isTrue(typeof promise.then === 'function');
        promise.then((valid) => {
            assert.isTrue(valid);
            assert.isUndefined(field.valid);
            done()
        })
    })
});
/*

 let f = new Field('field1');
 f.value === undefined
 f.setValue('test', (val) => {val === test})
 .then((val) => {
 val === test
 })
 f.value === test


 */