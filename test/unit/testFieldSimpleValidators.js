'use strict';
const assert = require('chai').assert;
const Field = require('./../../src/field');
const _ = require('lodash');
const EventBus = require('wolfy87-eventemitter');

describe('simple validators', function () {
    describe('direct result', function () {
        it('should set validation result into field immediately', function (done) {
            let validators = [
                new class isStringValidator {
                    validate(value) {
                        return _.isString(value);
                    }
                },
                new class isShortStringValidator {
                    validate(value) {
                        return _.isString(value) && value.length <= 10;
                    }
                },
                function (value) {
                    return value !== 'badString';
                }
            ];
            let field = new Field('testField', validators);
            field.setValue('veryVeryLongString')
            assert.equal(field.value, 'veryVeryLongString');
            assert.equal(field.validate(), false);
            assert.isFalse(field.valid);
            done()
        });
        it('should return validation result immediately', function (done) {
            let validators = [
                new class isStringValidator {
                    validate(value) {
                        return _.isString(value);
                    }
                },
                new class isShortStringValidator {
                    validate(value) {
                        return _.isString(value) && value.length <= 10;
                    }
                },
                function (value) {
                    return value !== 'badString';
                }
            ];
            let field = new Field('testField', validators);
            field.setValue('veryVeryLongString')
            assert.equal(field.value, 'veryVeryLongString');
            assert.equal(field.isValid(), false);
            assert.isUndefined(field.valid);
            field.setValue('tinyString');
            assert.equal(field.value, 'tinyString');
            assert.equal(field.isValid(), true);
            assert.isUndefined(field.valid);
            field.setValue('badString');
            assert.equal(field.value, 'badString');
            assert.equal(field.isValid(), false);
            assert.isUndefined(field.valid);
            done()
        })
        it('should return validation result immediately - hierarchy of fields', function (done) {
            let subValidators1 = [
                new class isStringValidator {
                    validate(value) {
                        return _.isString(value);
                    }
                },
            ];
            let subValidators2 = [
                new class isShortStringValidator {
                    validate(value) {
                        return _.isString(value) && value.length <= 10;
                    }
                },
            ];
            let subValidators3 = [
                new class isFiveValidator {
                    validate(value) {
                        return value === 5;
                    }
                },
            ];
            let groupFiled = new Field('testField');
            let subField1 = new Field('subField1', subValidators1);
            let subField2 = new Field('subField2');
            let subSubField1 =new Field('subsSubField1', subValidators3)
            groupFiled.addChild(subField1);
            groupFiled.addChild(subField2);
            subField2.addChild(subSubField1);
            subField1.setValue('5')
            subSubField1.setValue(5);

            assert.equal(groupFiled.isValid(), true);
            done()
        })
    })
    describe('using callback', function () {
        it('should set validation result into field immediately and call callback', function (done) {
            let validators = [
                new class isStringValidator {
                    validate(value) {
                        return _.isString(value);
                    }
                },
                new class isShortStringValidator {
                    validate(value) {
                        return _.isString(value) && value.length <= 10;
                    }
                },
                function (value) {
                    return value !== 'badString';
                }
            ];
            let field = new Field('testField', validators);
            field.setValue('veryVeryLongString')
            assert.equal(field.value, 'veryVeryLongString');
            assert.equal(field.validate((result) => {
                assert.isFalse(result);
            }), false);
            assert.isFalse(field.valid);
            done()
        });
    })
    describe('using event', function () {
        it('should set validation result into field immediately and emit an event', function (done) {
            let bus = new EventBus();
            bus.on('after-validate', (valid) => {
                assert.isFalse(valid);
                done();
            })
            let validators = [
                new class isStringValidator {
                    validate(value) {
                        return _.isString(value);
                    }
                },
                new class isShortStringValidator {
                    validate(value) {
                        return _.isString(value) && value.length <= 10;
                    }
                },
                function (value) {
                    return value !== 'badString';
                }
            ];
            let field = new Field('testField', validators, {eventEmitter: bus});
            field.setValue('veryVeryLongString')
            assert.equal(field.value, 'veryVeryLongString');
            assert.equal(field.validate(), false);
            assert.isFalse(field.valid);
        });
    })
});